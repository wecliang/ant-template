# ant-template

## 介绍

可视化搭建，模板快速修改，拖拽生成 react 源码

## 适用工程

[Antd@4x](https://ant.design/index-cn)  
[typescript](https://www.tslang.cn/docs/home.html)

## 快速体验

[在线体验地址](http://kit.tinvapi.com/make)

目前已实现大部分 Antd 组件的拖拽使用，拖拽组件快速完成页面搭建，拷贝生成源码在项目中可直接使用。
![1](https://kit.tinvapi.com/img/12312543523.png)
![2](https://kit.tinvapi.com/img/23424343534521.png)

## 安装说明
1. 克隆工程
```
git clone https://gitee.com/wecliang/ant-template.git
```
2. 安装依赖
```
$ cd ant-template && yarn
// 或
$ cd ant-template && npm install
```
3. 运行工程
```
yarn start
// 或
npm run start
```
4. 打包工程
```
yarn build
// 或
npm run build
```